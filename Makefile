RED 	= \033[0;31m
GREEN 	= \033[1;32m
ORANGE 	= \033[0;33m
GRAY 	= \033[1;30m
DEF 	= \033[0m

CC			= clang
CC_FLAGS	= -Wall -Wextra -Werror -O3 #-g

BIN_DIR		= bin
INCLUDE		= -I include/ -I libft/ -I libmlx/
LIBRARY		= -L./libft/ -lft -lpthread -lm -lmlx -framework OpenGL -framework AppKit  #libmlx/libmlx_Linux.a -lXext -lX11

SRC = 	src/main.c \
		src/algorithm.c \
		src/img_workers.c \
		src/ivent_handlers.c \
		src/ivent_handlers_2.c \
		src/file_workers.c

OBJ		 	= $(SRC:.c=.o)
EXECUTABLE	= $(BIN_DIR)/fractol

all: compile_lib create_bin_dir $(EXECUTABLE)
	@ echo "${GREEN}Compile binary file was successful${DEF}"

create_bin_dir:
	@ mkdir -p bin

compile_lib:
	@ make -s -C libft/

$(EXECUTABLE): $(OBJ)
	@ $(CC) $(CC_FLAGS) $(OBJ) -o $(EXECUTABLE) $(LIBRARY)

%.o : %.c
	@ $(CC) $(CC_FLAGS) $(INCLUDE) -o $@ -c $<
	@ echo "${GRAY}Compile object files${DEF}"

clean:
	@ rm -f $(OBJ)
	@ make clean -s -C libft/
	@ echo "${ORANGE}Object files was successfuly remowed${DEF}"

fclean: clean
	@ rm -rf $(BIN_DIR)
	@ make fclean -s -C libft/
	@ echo "${RED}Binary file was successfuly remowed${DEF}"

re: fclean all