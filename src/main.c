#include "fractol.h"

static int	usage(const char *prog_name)
{
	ft_putstr(GREEN);
	ft_putstr("Usgae: ");
	ft_putstr(prog_name);
	ft_putendl(" [fractals]");
	ft_putstr(YELLOW);
	ft_putstr("	--> ");
	ft_putendl(MALDELBROT);
	ft_putstr("	--> ");
	ft_putendl(JULIA);
	ft_putstr("	--> ");
	ft_putendl(BURNINGSHIP);
	ft_putstr(DEF);
	return (EXIT_FAILURE);
}

static void	error(const char *error_msg, const char *additional_info,
					int need_exit)
{
	ft_putstr(RED);
	ft_putstr("ERROR: ");
	if (additional_info)
		ft_putstr(additional_info);
	ft_putendl(error_msg);
	ft_putstr(DEF);
	if (need_exit)
		exit(EXIT_FAILURE);
}

static void	get_fractol_conf(const char *name, t_all_info *i)
{
	double	x_range;
	double	y_range;

	if (!ft_strcmp(name, MALDELBROT))
		i->stat = (t_stat){-2.0f, 2.0f, -1.0f, 1.0f, -0.5f, 0.0f, 100.0f,
				1.0f, {0, 0}, 0, 0, 0, T_MALDELBROT};
	else if (!ft_strcmp(name, JULIA))
		i->stat = (t_stat){-2.0f, 2.0f, -2.0f, 2.0f, 0.0f, 0.0f, 100.0f,
				1.0f, {0, 0}, 0, 0, 0, T_JULIA};
	else if (!ft_strcmp(name, BURNINGSHIP))
		i->stat = (t_stat){-2.0f, 2.0f, -2.0f, 1.0f, -0.5f, 0.0f, 100.0f,
					1.0f, {0, 0}, 0, 0, 0, T_BURNINGSHIP};
	x_range = (i->stat.x_max - i->stat.x_min);
	y_range = (i->stat.y_max - i->stat.y_min);
	if (x_range / y_range >= (float)WIDTH / HEIGHT)
	{
		i->stat.y_min = -(x_range * HEIGHT / WIDTH / 2);
		i->stat.y_max = x_range * HEIGHT / WIDTH / 2;
	}
	else
	{
		i->stat.x_min = -(y_range * WIDTH / HEIGHT / 2);
		i->stat.x_max = y_range * WIDTH / HEIGHT / 2;
	}
}

static void	initialize(char *fractol)
{
	t_all_info	inf;

	ft_bzero(&inf, sizeof(inf));
	if ((inf.mlx.handler = mlx_init()) == NULL)
		error("can't initialize mlx", NULL, 1);
	if ((inf.mlx.window = mlx_new_window(inf.mlx.handler, WIDTH,
										HEIGHT, fractol)) == NULL)
		error("can't create mlx window", NULL, 1);
	if ((inf.mlx.img = mlx_new_image(inf.mlx.handler, WIDTH, HEIGHT)) == NULL)
		error("can't create mlx image", NULL, 1);
	if ((inf.mlx.img_data = mlx_get_data_addr(inf.mlx.img,
			&inf.mlx.img_info.byte_per_pixel, &inf.mlx.img_info.line_size,
			&inf.mlx.img_info.pixel_endian)) == NULL)
		error("can't get mlx image data", NULL, 1);
	get_fractol_conf(fractol, &inf);
	inf.n_thread = sysconf(_SC_NPROCESSORS_ONLN);
	if ((inf.t_arr = (t_thread_inf*)malloc(sizeof(t_thread_inf) * inf.n_thread)) == NULL)
		error("can't allocate memory for threads aray", NULL, 1);
	rendering(&inf);

	mlx_hook(inf.mlx.window, 17, 1L << 17, close_win_btn, NULL);
	mlx_hook(inf.mlx.window, 2, 5, key_handler, &inf);
	mlx_hook(inf.mlx.window, 4, 1L << 2, mouse_scrol, &inf);
	if (inf.stat.f_type == T_JULIA)
	{
		mlx_hook(inf.mlx.window, 5, 1L << 3, mouseup, &inf);
		mlx_hook(inf.mlx.window, 6, 1L << 6, mouse_change_pos, &inf);
	}
	mlx_loop(inf.mlx.handler);
}

int		main(int argc, char **argv)
{
	int	i;

	if (argc == 1)
		return (usage(argv[0]));
	i = 0;
	while (++i < argc)
		if (!ft_strcmp(argv[i], JULIA) || !ft_strcmp(argv[i], MALDELBROT)
			|| !ft_strcmp(argv[i], BURNINGSHIP))
		{
			if ((i + 1) < argc && fork() == 0)
			{
				close(STDIN_FILENO);
				close(STDERR_FILENO);
				close(STDOUT_FILENO);
				initialize(argv[i]);
			}
			else if ((i + 1) >= argc)
				initialize(argv[i]);
		}
		else
			error(" - invalid fractol name", argv[i], 0);
	return (usage(argv[0]));
}