#include "fractol.h"
#include <stdio.h>

static char	*generate_name(int num)
{
	char	*tmp;
	char	*tmp_2;

	tmp = ft_itoa(num);
	tmp_2 = ft_strjoin("screenshot_", tmp);
	free(tmp);
	tmp = ft_strjoin(tmp_2, ".ppm");
	free(tmp_2);
	return (tmp);
}

void		save_screenshot(t_all_info *inf)
{
	t_color			r_color;
	int				i;
	char			*tmp;
	FILE 			*fp;
	unsigned char	color[3];

	tmp = generate_name(inf->file_num++);
	fp = fopen(tmp, "wb");
	free(tmp);
  	(void) fprintf(fp, "P6\n%d %d\n255\n", WIDTH, HEIGHT);
	i = -1;
	while (++i < WIDTH * HEIGHT) {
		r_color.color = (inf->smooth) ?
			get_smooth_color(inf->last_img[i], inf->stat.depth) :
			get_liner_color(inf->last_img[i], inf->stat.depth);
		color[0] = (unsigned char)r_color.rgba.r;
		color[1] = (unsigned char)r_color.rgba.g;
		color[2] = (unsigned char)r_color.rgba.b;
		(void) fwrite(color, 1, 3, fp);

	}
	(void) fclose(fp);
}
