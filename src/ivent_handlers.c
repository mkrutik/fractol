#include "fractol.h"

int			close_win_btn(void *ptr)
{
	(void)ptr;
	exit(EXIT_SUCCESS);
	return (EXIT_SUCCESS);
}

void		zooming(t_all_info *inf, int x, int y, double z)
{
	double	x_range;
	double	y_range;

	x_range = (inf->stat.x_max - inf->stat.x_min);
	y_range = (inf->stat.y_max - inf->stat.y_min);
	inf->stat.x_pos -= ((double)x / WIDTH) * ((x_range * inf->stat.zoom * z)
		- (x_range * inf->stat.zoom));
	inf->stat.y_pos -= ((double)y / HEIGHT) * ((y_range * inf->stat.zoom * z)
		- (y_range * inf->stat.zoom));
	inf->stat.zoom = inf->stat.zoom * z;
}

static void moving(t_all_info *inf, int key)
{
	double mapped_w;
	double mapped_h;

	mapped_w = (inf->stat.x_max - inf->stat.x_min) * inf->stat.zoom;
	mapped_h = (inf->stat.y_max - inf->stat.y_min) * inf->stat.zoom;
	if (key == KEY_UP)
		inf->stat.y_pos -= mapped_h * 0.05f;
	else if (key == KEY_DOWN)
		inf->stat.y_pos += mapped_h * 0.05f;
	else if (key == KEY_LEFT)
		inf->stat.x_pos -= mapped_w * 0.05f;
	else if (key == KEY_RIGHT)
		inf->stat.x_pos += mapped_w * 0.05f;
}

static void chage_coloring(t_all_info *inf)
{
	int	y;
	int	x;

	inf->smooth = !inf->smooth;
	clear_img(inf->mlx.img_data, &inf->mlx.img_info);
	y = -1;
	while (++y < HEIGHT && (x = -1) == -1)
		while (++x < WIDTH)
			put_pixel(inf, inf->mlx.img_data, x, y, inf->last_img[y * WIDTH + x]);
	mlx_put_image_to_window(inf->mlx.handler, inf->mlx.window, inf->mlx.img, 0, 0);
}

int		key_handler(int key, t_all_info *inf)
{
	int re_draw;

	re_draw = 0;
	if (key == KEY_ESC)
		exit(EXIT_SUCCESS);
	else if (key == KEY_S)
		save_screenshot(inf);
	else if (key == KEY_MULT && (re_draw = 1))
		inf->stat.depth = inf->stat.depth * 2;
	else if (key == KEY_DIV && (inf->stat.depth / 2) >= 2 && (re_draw = 1))
		inf->stat.depth = inf->stat.depth / 2;
	else if ((key == KEY_PLUS || key == KEY_MINUS) && (re_draw = 1))
		zooming(inf, WIDTH_CENTR, HEIGHT_CENTR,
				(key == KEY_PLUS) ? (1 / 1.1f) : 1.1f);
	else if ((key == KEY_UP || key == KEY_DOWN || key == KEY_LEFT
			|| key == KEY_RIGHT) && (re_draw = 1))
		moving(inf, key);
	else if (key == KEY_SPACE)
		chage_coloring(inf);
	if (re_draw)
		rendering(inf);
	return (EXIT_SUCCESS);
}
