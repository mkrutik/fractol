#include "fractol.h"

void clear_img(char *img, const t_img_inf *img_inf)
{
	ft_bzero(img, HEIGHT * img_inf->line_size);
}

int 	get_liner_color(int iter, double depth)
{
	t_color res_color;
	int tmp;

	tmp = (int)(((double)iter / depth) * (double)powf(256, 3));
	res_color.rgba.b = tmp / (powf(256, 2));
	tmp = tmp - res_color.rgba.b * powf(256, 2);
	res_color.rgba.r = tmp / 256;
	res_color.rgba.g = tmp - res_color.rgba.r * 256;
	return (res_color.color);
}

int 	get_smooth_color(int iter, double depth)
{
	t_color res_color;
	double t;

	t = (double)iter / depth;
	res_color.rgba.b = (int)(9 * (1 - t) * powf(t, 3) * 255);
	res_color.rgba.r = (int)(15 * powf((1 - t), 2) * powf(t, 2) * 255);
	res_color.rgba.g = (int)(8.5 * powf((1 - t), 3) * t * 255);
	return (res_color.color);
}

void put_pixel(const t_all_info *inf, char *img, int x, int y, int iter)
{
	int *pixel;
	int color;

	if (iter == inf->stat.depth)
		color = 0;
	else
	{
		if (inf->smooth)
			color = get_smooth_color(iter, inf->stat.depth);
		else
			color = get_liner_color(iter, inf->stat.depth);
	}
	if (x < 0 || x >= WIDTH || y < 0 || y >= HEIGHT)
		return ;
	pixel = (int *)(img + (x * (inf->mlx.img_info.byte_per_pixel / 8)  +
			y * inf->mlx.img_info.line_size));
	*pixel = color;
}
