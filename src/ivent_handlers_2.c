#include "fractol.h"

int		mouse_scrol(int key, int x, int y, t_all_info *inf)
{
	if (key == 4 || key == 5)
	{
		zooming(inf, x, y, (key == 4) ? (1 / 1.1f) : 1.1f);
		rendering(inf);
	}
	if (y < 0)
		return (0);
	inf->stat.is_down |= 1 << key;
	return (0);
}

int		mouseup(int key, int x, int y, t_all_info *inf)
{
	(void)x;
	(void)y;
	inf->stat.is_down &= ~(1 << key);
	return (0);
}

int		mouse_change_pos(int x, int y, t_all_info *inf)
{
	double w;
	double h;

	int lastx;
	int lasty;

	lastx = inf->stat.mouse_x;
	lasty = inf->stat.mouse_y;
	inf->stat.mouse_x = x;
	inf->stat.mouse_y = y;
	inf->stat.mouse = point_to_complex(x, y, &inf->stat);
	if (inf->stat.is_down & (1 << 1))
	{
		w = (inf->stat.x_max - inf->stat.x_min) * inf->stat. zoom;
		h = (inf->stat.y_max - inf->stat.y_min) * inf->stat. zoom;
		inf->stat.x_pos += (double)(lastx - inf->stat.mouse_x)
			/ WIDTH * w;
		inf->stat.y_pos += (double)(lasty - inf->stat.mouse_y)
			/ HEIGHT * h;
	}
	rendering(inf);
	return (0);
}

void	register_hook(t_all_info *inf)
{
	mlx_hook(inf->mlx.window, 17, 1L << 17, close_win_btn, NULL);
	mlx_hook(inf->mlx.window, 2, 5, key_handler, &inf);
	mlx_hook(inf->mlx.window, 4, 1L << 2, mouse_scrol, &inf);
	if (inf->stat.f_type == T_JULIA)
	{
		mlx_hook(inf->mlx.window, 5, 1L << 3, mouseup, &inf);
		mlx_hook(inf->mlx.window, 6, 1L << 6, mouse_change_pos, &inf);
	}
}