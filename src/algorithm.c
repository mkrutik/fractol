#include "fractol.h"

static int		calc_point(t_complex_num z, t_complex_num c, double depth,
							uint8_t need_fabs)
{
	t_complex_num	tmp;
	int				iteration;

	iteration = -1;
	while (++iteration < depth && pow(z.real, 2) + pow(z.img, 2) < 1.0E16)
	{
		if (need_fabs)
		{
			z.real = fabs(z.real);
			z.img = fabs(z.img);
		}
		tmp.real = pow(z.real, 2) - pow(z.img, 2) + c.real;
		tmp.img = z.real * z.img * 2 + c.img;
		if (tmp.real == z.real && tmp.img == z.img)
			return (depth);
		z.real = tmp.real;
		z.img = tmp.img;
	}
	return (iteration);
}

t_complex_num	point_to_complex(int x, int y, const t_stat *state)
{
	t_complex_num res;

	res.real = (double)x / WIDTH * (state->x_max - state->x_min) *
			state->zoom + state->x_pos + state->x_min;
	res.img = (double)y / HEIGHT * (state->y_max - state->y_min) *
			state->zoom + state->y_pos + state->y_min;
	return (res);
}

void			*proccess(void *ptr)
{
	int					x;
	int					y;
	const t_thread_inf 	*d;

	t_complex_num		tmp;
	d = (t_thread_inf*)ptr;
	y = (HEIGHT / d->inf->n_thread * d->num) - 1;
	while(++y < (HEIGHT / d->inf->n_thread * (d->num + 1)) && (x = -1) == -1)
		while(++x < WIDTH)
		{
			tmp = point_to_complex(x, y, &d->inf->stat);
			if (d->inf->stat.f_type == T_MALDELBROT)
				d->inf->last_img[y * WIDTH + x] =
				calc_point(tmp, tmp, d->inf->stat.depth, 0);
			else if (d->inf->stat.f_type == T_JULIA)
				d->inf->last_img[y * WIDTH + x] =
				calc_point(tmp, d->inf->stat.mouse, d->inf->stat.depth, 0);
			else if (d->inf->stat.f_type == T_BURNINGSHIP)
				d->inf->last_img[y * WIDTH + x] =
				calc_point(tmp, tmp, d->inf->stat.depth, 1);
		}
	return (NULL);
}

void			rendering(t_all_info *inf)
{
	int 		x;
	int 		y;

	x = -1;
	while (++x < inf->n_thread)
	{
		inf->t_arr[x].inf = inf;
		inf->t_arr[x].num = x;
		pthread_create(&inf->t_arr[x].thread, NULL, proccess, &inf->t_arr[x]); // TODO: is it ok ?
	}
	x = -1;
	while (++x < inf->n_thread)
		pthread_join(inf->t_arr[x].thread, NULL);
	clear_img(inf->mlx.img_data, &inf->mlx.img_info);
	y = -1;
	while (++y < HEIGHT && (x = -1) == -1)
		while (++x < WIDTH)
			put_pixel(inf, inf->mlx.img_data, x, y, inf->last_img[y * WIDTH + x]);
	mlx_put_image_to_window(inf->mlx.handler, inf->mlx.window, inf->mlx.img, 0, 0);
}