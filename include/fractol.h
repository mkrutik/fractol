#ifndef FRACTOL_H
# define FRACTOL_H

# include "libft.h"
# include "mlx.h"

# include <fcntl.h>
# include <inttypes.h>
# include <pthread.h>
# include <math.h>

/*
** terminal color
*/
# define DEF     "\e[0;0m"
# define GREEN   "\e[1;32m"
# define RED     "\e[1;31m"
# define YELLOW  "\e[1;33m"
# define CYAN    "\e[0;35m"
/*
** Key codes
*/
# define KEY_ESC		53
# define KEY_SPACE		49
# define KEY_S			1
# define KEY_UP			126
# define KEY_DOWN		125
# define KEY_LEFT		123
# define KEY_RIGHT		124
# define KEY_MULT		24
# define KEY_DIV		27
# define KEY_PLUS		6
# define KEY_MINUS		0
/*
** Fractals name
*/
# define JULIA			"Julia"
# define MALDELBROT		"Mandelbrot"
# define BURNINGSHIP 	"Burningship"
/*
** Window size
*/
# define HEIGHT 768
# define WIDTH 	1024
# define HEIGHT_CENTR (HEIGHT / 2)
# define WIDTH_CENTR (WIDTH / 2)

typedef struct		s_img_inf {
	int				line_size;
	int				byte_per_pixel;
	int				pixel_endian;
} 					t_img_inf;

typedef struct		s_mlx_info
{
	void			*handler;
	void			*window;
	void			*img;
	char			*img_data;

	t_img_inf		img_info;
}					t_mlx_info;

typedef	struct		s_complex_num {
	double			real;
	double			img;
}					t_complex_num;

typedef enum		e_f_type {
	T_MALDELBROT,
	T_JULIA,
	T_BURNINGSHIP
}					t_f_type;

typedef struct 		s_rgba {
	uint8_t			r;
	uint8_t 		g;
	uint8_t 		b;
	uint8_t			a;
}					t_rgba;

typedef	union 		u_color
{
	t_rgba			rgba;
	int				color;
}					t_color;

typedef struct		s_state {
	double			x_min;
	double			x_max;
	double			y_min;
	double			y_max;

	double			x_pos;
	double			y_pos;

	double			depth;
	double			zoom;

	t_complex_num	mouse;
	int				mouse_x;
	int 			mouse_y;
	uint8_t			is_down;

	t_f_type		f_type;
}					t_stat;

typedef struct			s_thread_inf {
	pthread_t			thread;
	int					num;
	struct s_all_info 	*inf;
} 						t_thread_inf;

typedef struct			s_all_info {
	t_mlx_info 			mlx;

	int					n_thread;
	t_thread_inf		*t_arr;

	int					last_img[WIDTH * HEIGHT];

	t_stat				stat;
	uint8_t				smooth;
	int					file_num;

}						t_all_info;

t_complex_num			point_to_complex(int x, int y, const t_stat *state);
void					rendering(t_all_info *inf);
/*
** Image helpers
*/
void					clear_img(char *img, const t_img_inf *img_inf);
void					put_pixel(const t_all_info *inf, char *img, int x, int y, int color);
int 					get_liner_color(int iter, double depth);
int 					get_smooth_color(int iter, double depth);
/*
** Ivent handlers
*/
void					register_hook(t_all_info *inf);
int						close_win_btn(void *ptr);
int						key_handler(int key, t_all_info *inf);
void					zooming(t_all_info *inf, int x, int y, double z);
int						mouse_scrol(int key, int x, int y, t_all_info *inf);
int						mouse_change_pos(int x, int y, t_all_info *inf);
int						mouseup(int key, int x, int y, t_all_info *inf);
void					save_screenshot(t_all_info *inf);

#endif
